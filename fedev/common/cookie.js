/**
  cookie相关操作
**/
class Cookie{
	  constructor(){

	  }
	  set(name,value,second){			
			let exp = new Date();

			exp.setTime(exp.getTime() + second*1000);
			document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
	   }
	  get(name){
			let arr,
			    reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");

			if(arr=document.cookie.match(reg))
			   return unescape(arr[2]);
			else{
				return null;
			}
		}
	  remove(name){
			let exp = new Date();
			exp.setTime(exp.getTime() - 1);
			let cval=getCookie(name);

			if(cval!=null){
				document.cookie= name + "="+cval+";expires="+exp.toGMTString();
			}			
		}
}

module.exports=new Cookie();