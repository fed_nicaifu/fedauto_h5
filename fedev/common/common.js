class Common{
	constructor(){
		this.init();
	}
	init(){
	  let self=this;

	  self.urlConfig={
	  	 login:"http://123.56.192.237:8081/login/loginajax"
	  }	  
	}
	ajax(path,method,type,params,callback){
		
		$.ajax({
           type: method,
           url: path,
           dataType:type||'json',
           data:params||{},
           timeout: 300,
           success: function(data){
                   callback&&callback(data);                         
                },
           error: function(xhr, type){                  
                 $.dialog({
			        content:'网络不好，请重试~',
			        button:["知道了"]
			     });           
              }
        })
	}
}

module.exports=new Common()