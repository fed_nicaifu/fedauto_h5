import common from '../common/common.js'
import cookie from '../common/cookie.js'

class Login{
	constructor(){
		this.init();
	}
	init(){
		$(".btn-login").click(function(){

			let userName=$("input[name=userName]").val();
            let pwd=$("input[name=password]").val();

            if(!userName) {
            	$.dialog({
			        content:'请填写用户名',
			        button:["知道了"]
			    });
			    return;
            }
            if(!pwd) {
            	$.dialog({
			        content:'请填写密码',
			        button:["知道了"]
			    });
			    return;
            }
            
            let url=common.urlConfig.login;
            common.ajax(url,"post","json",{userName:userName,passport:pwd},function(data){
                 if (data.errno==0) {
                 	let cookieName="h5_ncf_tc_sys";
                    cookie.set(cookieName,data.encrypt,60*20);
                 }else{
                 	$.dialog({
				        content:data.errmsg,
				        button:["知道了"]
				    });
                 }
            })
		})
	}
}

$(function(){
	new Login();
})