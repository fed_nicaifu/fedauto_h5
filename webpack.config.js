let path = require("path");
let webpack = require("webpack");

module.exports = {
	entry:{
		login:'./fedev/page/login.js',
	},
	output: {
		path: path.join(__dirname, "/public/scripts/"),
		filename: "[name].bundle.js",
	},
	module: {
		loaders: [
			{
				test: /\.js$/, 
				loader: 'babel-loader',
				exclude: /(node_modules|public)/,
				query: {
				  presets: ['es2015']
				}				
			},
			
			{
				test: /\.css$/,
				exclude: /(node_modules)/, 
				loader: "style-loader!css-loader"
			},
			{
				test: /\.html$/,
				exclude: /(node_modules)/, 
				loader: "html-loader"
			}
		]
	},
	// plugins: [
	//     new webpack.optimize.UglifyJsPlugin({
	//       compress: {
	//         warnings: false
	//       }
	//     })
 //  ]
};
