/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Common = function () {
	function Common() {
		_classCallCheck(this, Common);

		this.init();
	}

	_createClass(Common, [{
		key: 'init',
		value: function init() {
			var self = this;

			self.urlConfig = {
				login: "http://123.56.192.237:8081/login/loginajax"
			};
		}
	}, {
		key: 'ajax',
		value: function ajax(path, method, type, params, callback) {

			$.ajax({
				type: method,
				url: path,
				dataType: type || 'json',
				data: params || {},
				timeout: 300,
				success: function success(data) {
					callback && callback(data);
				},
				error: function error(xhr, type) {
					$.dialog({
						content: '网络不好，请重试~',
						button: ["知道了"]
					});
				}
			});
		}
	}]);

	return Common;
}();

module.exports = new Common();

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
  cookie相关操作
**/
var Cookie = function () {
	function Cookie() {
		_classCallCheck(this, Cookie);
	}

	_createClass(Cookie, [{
		key: "set",
		value: function set(name, value, second) {
			var exp = new Date();

			exp.setTime(exp.getTime() + second * 1000);
			document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
		}
	}, {
		key: "get",
		value: function get(name) {
			var arr = void 0,
			    reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");

			if (arr = document.cookie.match(reg)) return unescape(arr[2]);else {
				return null;
			}
		}
	}, {
		key: "remove",
		value: function remove(name) {
			var exp = new Date();
			exp.setTime(exp.getTime() - 1);
			var cval = getCookie(name);

			if (cval != null) {
				document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
			}
		}
	}]);

	return Cookie;
}();

module.exports = new Cookie();

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _common = __webpack_require__(0);

var _common2 = _interopRequireDefault(_common);

var _cookie = __webpack_require__(1);

var _cookie2 = _interopRequireDefault(_cookie);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Login = function () {
	function Login() {
		_classCallCheck(this, Login);

		this.init();
	}

	_createClass(Login, [{
		key: 'init',
		value: function init() {
			$(".btn-login").click(function () {

				var userName = $("input[name=userName]").val();
				var pwd = $("input[name=password]").val();

				if (!userName) {
					$.dialog({
						content: '请填写用户名',
						button: ["知道了"]
					});
					return;
				}
				if (!pwd) {
					$.dialog({
						content: '请填写密码',
						button: ["知道了"]
					});
					return;
				}

				var url = _common2.default.urlConfig.login;
				_common2.default.ajax(url, "post", "json", { userName: userName, passport: pwd }, function (data) {
					if (data.errno == 0) {
						var cookieName = "h5_ncf_tc_sys";
						_cookie2.default.set(cookieName, data.encrypt, 60 * 20);
					} else {
						$.dialog({
							content: data.errmsg,
							button: ["知道了"]
						});
					}
				});
			});
		}
	}]);

	return Login;
}();

$(function () {
	new Login();
});

/***/ })
/******/ ]);